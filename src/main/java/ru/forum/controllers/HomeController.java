package ru.forum.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by AlexeyKh89 on 14.12.2015.
 */
public class HomeController {
    @RequestMapping(path="/", method= RequestMethod.GET)
    public String getHome() {
        return "Hello user!";
    }
}
