package ru.forum.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by AlexeyKh89 on 14.12.2015.
 */
@RestController
public class MessageController {

    @RequestMapping(path="/message", method= RequestMethod.GET)
    public String getMessage() {
        return "Get message";
    }
	
	@RequestMapping(path="/message", method=RequestMethod.POST)
    public String addOrEditMessage() {
        return "Post message";
    }

}